//
//  InternetUtility.m
//  BBMPinBook
//
//  Created by CRATERZONE on 28/12/13.
//  Copyright (c) 2013 CRATERZONE. All rights reserved.
//

#import "InternetUtility.h"

@implementation InternetUtility
@synthesize delegate;

+ (InternetUtility *)sharedSingleton
{
    static InternetUtility *sharedSingleton;
    
    @synchronized(self)
    {
        if (!sharedSingleton)
            sharedSingleton = [[InternetUtility alloc] init];
        
        return sharedSingleton;
    }
}


+(BOOL)testInternetConnection {
    BOOL isInternetAvailable = FALSE;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        isInternetAvailable = TRUE;
    } else {
        //there-is-no-connection warning
        isInternetAvailable = FALSE;
       /* UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"err", nil)                                                                                            message:NSLocalizedString(@"error_connection_prob", nil)
                                                      delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"OK", nil];
            [alertView show];*/
            return isInternetAvailable;
    }
    return isInternetAvailable;
}


-(void)reachabilityNotifier
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    reachNotifier = [Reachability reachabilityForInternetConnection];
    [reachNotifier startNotifier];
}
-(void) checkNetworkStatus:(NSNotification *)notice
{
    
    NetworkStatus internetStatus = [reachNotifier currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            
            NSLog(@"The internet is down.");
            
           
            
            break;
        }
        case ReachableViaWiFi:
        {
           [self.delegate networkReachableMethod];
            NSLog(@"The internet is working via WIFI.");
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            break;
        }
    }
    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)
    {
        case NotReachable:
        {
            NSLog(@"A gateway to the host server is down.");
            break;
        }
        case ReachableViaWiFi:
        {
            [self.delegate networkReachableMethod];
            
            NSLog(@"A gateway to the host server is working via WIFI.");
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"A gateway to the host server is working via WWAN.");
            break;
        }
    }
}

@end
