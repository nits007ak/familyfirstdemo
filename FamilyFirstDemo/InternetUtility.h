//
//  InternetUtility.h
//  BBMPinBook
//
//  Created by CRATERZONE on 28/12/13.
//  Copyright (c) 2013 CRATERZONE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"


@protocol NetworkAppearanceProtocal <NSObject>

-(void)networkReachableMethod;
@end


@interface InternetUtility : NSObject
{
    Reachability * reachNotifier;
    Reachability* hostReachable;
}

@property (nonatomic, retain)id<NetworkAppearanceProtocal>delegate;

+(BOOL)testInternetConnection;
-(void)reachabilityNotifier;
-(void) checkNetworkStatus:(NSNotification *)notice;
+(InternetUtility *)sharedSingleton;



@end
