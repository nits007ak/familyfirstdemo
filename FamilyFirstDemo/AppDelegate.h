//
//  AppDelegate.h
//  FamilyFirstDemo
//
//  Created by CraterZone on 10/02/14.
//  Copyright (c) 2014 craterzone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate> {
    
    LocationViewController *locationView;
    NSMutableData *responseData;
    CLLocation *currentLocation;
}

@property (strong, nonatomic) UIWindow *window;

- (void)failIfSimulator;


@end
