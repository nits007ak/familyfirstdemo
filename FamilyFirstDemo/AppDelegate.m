//
//  AppDelegate.m
//  FamilyFirstDemo
//
//  Created by CraterZone on 10/02/14.
//  Copyright (c) 2014 craterzone. All rights reserved.
//

#import "AppDelegate.h"
#import "UAConfig.h"
#import "UAirship.h"
#import "UAPush.h"
#import "UAAnalytics.h"
#import "InternetUtility.h"
#import "ASIHTTPRequest.h"

#define UPDATE_LOCATION_SERVICE @"http://192.168.1.13:8080/locationUpdateDemo/location/update?"
#define HTTP_REQUEST_GET_METHOD @"GET"
#define HTTP_REQUEST_HEADER @"Accept"
#define HTTP_REQUEST_HEADER_VALUE @"application/json"
#define HTTP_REQUEST_CONTENTTYPE @"content-type"
#define HTTP_REQUEST_CONTENTTYPE_VALUE @"application/json"
#define USER_AGENT @"User-Agent"
#define DEVICE @"iPhone"


@interface AppDelegate() {
    NSDate* _lastSentUpdateAt;
    NSTimer *timer;
    UIBackgroundTaskIdentifier bgTask;
    BOOL isLocationServiceStart;
}
@property (nonatomic, strong) CLLocationManager *locationManager;

@end


@implementation AppDelegate
@synthesize locationManager;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    locationView = [[LocationViewController alloc] initWithNibName:@"LocationViewController" bundle:nil];
    
    // Override point for customization after application launch
    [self.window setRootViewController:locationView];
    [self.window makeKeyAndVisible];
    
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLLocationAccuracyBest;
    timer = [NSTimer scheduledTimerWithTimeInterval:60
                                             target:self
                                           selector:@selector(startUpdatingLocation)
                                           userInfo:nil
                                            repeats:YES];
    
    // Display a UIAlertView warning developers that push notifications do not work in the simulator
    // You should remove this in your app.
    [self failIfSimulator];
    
    // This prevents the UA Library from registering with UIApplication by default. This will allow
    // you to prompt your users at a later time. This gives your app the opportunity to explain the
    // benefits of push or allows users to turn it on explicitly in a settings screen.
    //
    // If you just want everyone to immediately be prompted for push, you can
    // leave this line out.
    [UAPush setDefaultPushEnabledValue:NO];
    
    // Set log level for debugging config loading (optional)
    // It will be set to the value in the loaded config upon takeOff
    [UAirship setLogLevel:UALogLevelTrace];
    
    // Populate AirshipConfig.plist with your app's info from https://go.urbanairship.com
    // or set runtime properties here.
    UAConfig *config = [UAConfig defaultConfig];
    
    // You can then programatically override the plist values:
    // config.developmentAppKey = @"YourKey";
    // etc.
    
    // Call takeOff (which creates the UAirship singleton)
    [UAirship takeOff:config];
    
    // Print out the application configuration for debugging (optional)
    UA_LDEBUG(@"Config:\n%@", [config description]);
    
    // Set the icon badge to zero on startup (optional)
    [[UAPush shared] resetBadge];
    
    // Set the notification types required for the app (optional). With the default value of push set to no,
    // UAPush will record the desired remote notification types, but not register for
    // push notifications as mentioned above. When push is enabled at a later time, the registration
    // will occur normally. This value defaults to badge, alert and sound, so it's only necessary to
    // set it if you want to add or remove types.
    [UAPush shared].notificationTypes = (UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert);
    
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    //UA_LDEBUG(@"Application did become active.");
    
    // Set the icon badge to zero on resume (optional)
    [[UAPush shared] resetBadge];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    UA_LINFO(@"Received remote notification (in appDelegate): %@", userInfo);
    //reset badge
    [[UAPush shared] resetBadge];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    UA_LINFO(@"Received remote notification (in appDelegate): %@", userInfo);
    
    //[self sendDataToServer:currentLocation];
    // Reset the badge after a push is received in a active or inactive state
    if (application.applicationState != UIApplicationStateBackground) {
        //send new location data to server when app is suspended.
        isLocationServiceStart = YES;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
       // [self sendDataToServer:currentLocation];
        
//        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
//        if (localNotif) {
//            localNotif.alertBody = @"body test";
//            localNotif.alertAction = @"Notification Location";
//            localNotif.soundName = @"pig.caf";
//            localNotif.applicationIconBadgeNumber = 5;
//            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
//        }
        [[UAPush shared] resetBadge];
    }
    
    completionHandler(UIBackgroundFetchResultNoData);
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //[locationManager stopUpdatingLocation];
    UIApplication*    app = [UIApplication sharedApplication];
    
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:60
                                                  target:self
                                                selector:@selector(startUpdatingLocation)
                                                userInfo:nil
                                                 repeats:YES];
}

-(void)startUpdatingLocation {
    [locationManager stopUpdatingLocation];
    NSLog(@"start updating");
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    isLocationServiceStart = YES;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if(isLocationServiceStart) {
        currentLocation = newLocation;
        [self sendDataToServer:newLocation];
        isLocationServiceStart = NO;
    }
    
}

-(void) showAlertView {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"err", nil)                                                                                            message:NSLocalizedString(@"error_connection_prob", nil)
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    [alertView show];
}

/* This method sends updated latitude and longitude to server */
-(void) sendDataToServer:(CLLocation *)newLocation
{
    NSString *userId = @"125";
    NSString *latitude = [NSString stringWithFormat:@"%.8f", newLocation.coordinate.longitude];;
    NSString *longitude = [NSString stringWithFormat:@"%.8f", newLocation.coordinate.latitude];
    
    NSLog(@"Sending Data to Server");
    if([InternetUtility testInternetConnection]) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@userID=%@&latitude=%@&longitude=%@",UPDATE_LOCATION_SERVICE,userId,latitude,longitude]];
        responseData = [[NSMutableData alloc] init];
        if(url != nil) {
            ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
            //NSString *jsonRequest = [upgradeDict JSONRepresentation];
            //[request appendPostData:[jsonRequest dataUsingEncoding:NSUTF8StringEncoding]];
            //NSLog(@"jsonRequest is %@", request);
            [request setRequestMethod:HTTP_REQUEST_GET_METHOD];
            [request addRequestHeader:HTTP_REQUEST_HEADER value:HTTP_REQUEST_HEADER_VALUE];
            [request addRequestHeader:HTTP_REQUEST_CONTENTTYPE value:HTTP_REQUEST_CONTENTTYPE_VALUE];
            [request addRequestHeader:USER_AGENT value:DEVICE];
            [request setDelegate:self];
            [request setCompletionBlock:^{
                NSString *responseString = [request responseString];
                //NSLog(@"responseDict = %@",responseDict);
                UILocalNotification *localNotif = [[UILocalNotification alloc] init];
                if (localNotif) {
                    localNotif.alertBody = @"body test";
                    localNotif.alertAction = @"Notification Location";
                    localNotif.soundName = @"pig.caf";
                    localNotif.applicationIconBadgeNumber = 5;
                    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
                }
                NSLog(@"data send to server");
            }];
            [request setFailedBlock:^{
                NSLog (@"Unexpected error");
            }];
            [request startAsynchronous];
        }
    }
    //implement operation we need to do with new location object.
}



- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@",error);
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//    NSString *deviceTokenString = [NSString stringWithFormat:@"%@",deviceToken];
//    NSString *sub = [deviceTokenString stringByReplacingOccurrencesOfString:@">" withString:@""];
//    sub = [sub stringByReplacingOccurrencesOfString:@"<" withString:@""];
//    sub = [sub stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (void)failIfSimulator {
    if ([[[UIDevice currentDevice] model] rangeOfString:@"Simulator"].location != NSNotFound) {
        UIAlertView *someError = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                            message:@"You will not be able to receive push notifications in the simulator."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        
        // Let the UI finish launching first so it doesn't complain about the lack of a root view controller
        // Delay execution of the block for 1/2 second.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            [someError show];
        });
        
    }
}


@end
